package com.example.pre_examen_java;

public class ReciboNomina {
    // Declaración de variables de clase
    private int numRecibo;
    private String nombre;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private int puesto;
    private float impuestoPorc;

    // Métodos públicos
    public ReciboNomina(int numRecibo, String nombre, float horasTrabNormal, float horasTrabExtras, int puesto, float impuestoPorc) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
        this.impuestoPorc = impuestoPorc;
    }

    // Set & Get
    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public int getNumRecibo() {
        return this.numRecibo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabNormal() {
        return this.horasTrabNormal;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public float getHorasTrabExtras() {
        return this.horasTrabExtras;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public int getPuesto() {
        return this.puesto;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }

    public float getImpuestoPorc() {
        return this.impuestoPorc;
    }

    // Subtotal
    public float calcularSubtotal(int puesto, float horasTrabNormal, float horasTrabExtras) {
        float pagoBase = 200;
        float total = 0;
        // Calculando el pago base según el puesto
        if (puesto == 1) {
            pagoBase = (float) (pagoBase + (pagoBase * 0.20));
        } else if (puesto == 2) {
            pagoBase = (float) (pagoBase + (pagoBase * 0.50));
        } else if (puesto == 3) {
            pagoBase = (float) (pagoBase + (pagoBase * 1.00));
        } else {
            System.out.println("No se puede calcular");
        }

        // Calculando las horas extras (Se pagan al doble)
        total = ((pagoBase * horasTrabNormal) + (horasTrabExtras * pagoBase * 2));

        return total;

    }

    public float calcularImpuesto(int puesto, float horasTrabNormal, float horasTrabExtras) {
        float impuesto = (float) (calcularSubtotal(puesto, horasTrabNormal, horasTrabExtras) * 0.16);
        return impuesto;
    }

    public float calcularTotal(float subtotal, float impuesto) {
        float totalPagar = subtotal - impuesto;
        return totalPagar;
    }
}
